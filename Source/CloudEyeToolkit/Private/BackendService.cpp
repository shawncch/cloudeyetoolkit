// Fill out your copyright notice in the Description page of Project Settings.

#include "BackendService.h"

// Sets default values
ABackendService::ABackendService()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.

	Http = &FHttpModule::Get();
	Host = "http://cms.cloudeyelive.com:8080";
}

void ABackendService::BeginPlay()
{
	Super::BeginPlay();
}

/*
void ABackendService::CreateTableCommand(const FString &AppName, FString &Result)
{
	UNetworkingLibrary *HttpConnector;
	HttpConnector = NewObject<UNetworkingLibrary>(UNetworkingLibrary::StaticClass(), FName("Http Service"));
	HttpConnector->CreateAppTable(*AppName);
	Result = HttpConnector->GetCallback();
}
*/

void ABackendService::CreateAppTable(const FString &AppName)
{

	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &ABackendService::OnRequestCompleted);
	Request->SetURL(FString::Printf(TEXT("%s/nma_app/createAppTable.php?app=%s"), *Host, *AppName));
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->ProcessRequest();
}

void ABackendService::RemoveTable(const FString &AppName)
{
    TSharedRef<IHttpRequest> Request = Http->CreateRequest();
    Request->OnProcessRequestComplete().BindUObject(this, &ABackendService::OnRequestCompleted);
    Request->SetURL(FString::Printf(TEXT("%s/nma_app/removeTable.php?app=%s"), *Host, *AppName));
    Request->SetVerb("GET");
    Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
    Request->SetHeader("Content-Type",TEXT("application/json"));
    Request->ProcessRequest();
}


void ABackendService::AddTarget(const FString &AppName, const FString &TagName, const FString &BundleName, const FString &BundlePath, const FString &AssetFile, const FString &AppVersion)
{

	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &ABackendService::OnAddTargetCompleted);
	Request->SetURL(FString::Printf(TEXT("%s/nma_app/addTarget.php?app=%s&tag=%s&bundle_name=%s&bundle_path=%s&asset_file=%s&app_version=%s"), *Host, *AppName, *TagName, *BundleName, *BundlePath, *AssetFile, *AppVersion));
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->ProcessRequest();
}

void ABackendService::RemoveTarget(const FString &AppName, const FString &TagName)
{
    
    TSharedRef<IHttpRequest> Request = Http->CreateRequest();
    Request->OnProcessRequestComplete().BindUObject(this, &ABackendService::OnRequestCompleted);
    Request->SetURL(FString::Printf(TEXT("%s/nma_app/removeTarget.php?app=%s&tag=%s"), *Host, *AppName, *TagName));
    Request->SetVerb("GET");
    Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
    Request->SetHeader("Content-Type", TEXT("application/json"));
    Request->ProcessRequest();
    
}

void ABackendService::GetData(const FString &Action, const FString &AppName, const FString &TagName, const FString &AppVersion)
{
    
    TSharedRef<IHttpRequest> Request = Http->CreateRequest();
    
    FString ActionStr = *Action;
        if(ActionStr == "get_update_time")
            Request->OnProcessRequestComplete().BindUObject(this, &ABackendService::OnUpdateTimeCompleted);
        if(ActionStr == "get_tag_data")
            Request->OnProcessRequestComplete().BindUObject(this, &ABackendService::OnGetTargetsCompleted);
        if(ActionStr == "get_video_link")
            Request->OnProcessRequestComplete().BindUObject(this, &ABackendService::OnGetVideoPathCompleted);
        if(ActionStr == "get_version")
            Request->OnProcessRequestComplete().BindUObject(this, &ABackendService::OnGetLatestVersion);
        if(ActionStr == "update_version")
           Request->OnProcessRequestComplete().BindUObject(this, &ABackendService::OnUpdateLatestVersion);
     
    
    Request->SetURL(FString::Printf(TEXT("%s/Apps/%s/getdata.php?app=%s&action=%s&tag=%s&version=%s"), *Host, *AppName, *AppName, *Action, *TagName, *AppVersion));
    Request->SetVerb("GET");
    Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
    Request->SetHeader("Content-Type", TEXT("application/json"));
    Request->ProcessRequest();
    
}

 


void ABackendService::OnRequestCompleted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	TSharedPtr<FJsonObject> JsonObject;
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());
    
    ReceivedResult = Response->GetContentAsString();
    GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, ReceivedResult);
	
    if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//JsonObject->GetIntergerField("");
		// FString GetString = JsonObject->GetStringField("SomeStringFieldReturning");
		ReceivedResult =  FString(Response->GetContentAsString());
	 
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Green, ReceivedResult);
	}
}

void ABackendService::OnAddTargetCompleted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{

	TSharedPtr<FJsonObject> JsonObject;
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());
	GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, FString(Response->GetContentAsString()));
	ReceivedResult = FString(Response->GetContentAsString());

	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{ 	
		ReceivedResult = JsonObject->GetStringField("targets");
		//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Green, FString(ReceivedResult));
	}
}

void ABackendService::OnUpdateTimeCompleted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
    ReceivedResult ="";
    
    TSharedPtr<FJsonObject> JsonObject;
    TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());
    
    ReceivedResult = Response->GetContentAsString();
   
    GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, ReceivedResult);
    if (FJsonSerializer::Deserialize(Reader, JsonObject))
    {
        
        //ReceivedResult = FString(JsonObject->GetIntergerField(""));
       // FString GetString = JsonObject->GetStringField("SomeStringFieldReturning");
        GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Green, ReceivedResult);
    }
}

void ABackendService::OnGetTargetsCompleted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
    ReceivedResult ="";
    
    TSharedPtr<FJsonObject> JsonObject;
    TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());
    
    ReceivedResult = Response->GetContentAsString();
    
    GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, ReceivedResult);
    if (FJsonSerializer::Deserialize(Reader, JsonObject))
    {
        
        //ReceivedResult = FString(JsonObject->GetIntergerField(""));
        //ReceivedResult = JsonObject->GetArrayField("targets");
       //GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Green, ReceivedResult);
    }
}

void ABackendService::OnGetVideoPathCompleted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
    ReceivedResult ="";
    
    TSharedPtr<FJsonObject> JsonObject;
    TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());
    
    ReceivedResult = Response->GetContentAsString();
    
    GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, ReceivedResult);
    if (FJsonSerializer::Deserialize(Reader, JsonObject))
    {
        
        //ReceivedResult = FString(JsonObject->GetIntergerField(""));
       ReceivedResult= JsonObject->GetStringField("url");
        GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Green, ReceivedResult);
    }
}

void ABackendService::OnGetLatestVersion(FHttpRequestPtr Request, FHttpResponsePtr Respone, bool bWasSuccessful )
{
    ReceivedResult = "";
    
    TSharedPtr<FJsonObject> JsonObject;
    TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Respone->GetContentAsString());
    
    ReceivedResult = Respone->GetContentAsString();
    
    GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, ReceivedResult);
    if(FJsonSerializer::Deserialize(Reader, JsonObject))
    {
        //ReceivedResult = JsonObject->GetStringField("app_version");
        //GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Blue, ReceivedResult);
        //ReceivedResult = JsonObject->GetStringField("app_version");
    }
    
}

void ABackendService::OnUpdateLatestVersion(FHttpRequestPtr Request, FHttpResponsePtr Respone, bool bWasSuccessful)
{
    
    ReceivedResult = "";
    
    TSharedPtr<FJsonObject> JsonObject;
    TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Respone->GetContentAsString());
    
    ReceivedResult = Respone->GetContentAsString();
    
    GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Red, ReceivedResult);
   
    if(FJsonSerializer::Deserialize(Reader, JsonObject))
    {
        
        //GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Blue, ReceivedResult);
    }
}
