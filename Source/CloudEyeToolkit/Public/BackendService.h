// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "BackendService.generated.h"

UCLASS()
class CLOUDEYETOOLKIT_API ABackendService : public AActor
{
	GENERATED_BODY()

  protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

  public:
	ABackendService();
	FHttpModule *Http;
	FString Host;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "HttpService")
	FString ReceivedResult;

	//UFUNCTION(BlueprintCallable, Category = "HttpService")
	//static void CreateTableCommand(const FString &AppName, FString &Result);

	UFUNCTION(BlueprintCallable, Category = "HttpService")
	void CreateAppTable(const FString &AppName);
    
    UFUNCTION(BlueprintCallable, Category = "HttpService")
    void RemoveTable(const FString &AppName);

	UFUNCTION(BlueprintCallable, Category = "HttpService")
	void AddTarget(const FString &AppName, const FString &TagName, const FString &BundleName, const FString &BundlePath, const FString &AssetFile, const FString &AppVersion);
    
    UFUNCTION(BlueprintCallable, Category = "HttpService")
    void RemoveTarget(const FString &AppName, const FString &TagName);
    
    UFUNCTION(BlueprintCallable, Category = "HttpService")
    void GetData(const FString &Action, const FString &AppName, const FString &TagName, const FString &AppVersion);
    

	void OnRequestCompleted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void OnAddTargetCompleted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
    
    void OnUpdateTimeCompleted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
    void OnGetTargetsCompleted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
    void OnGetVideoPathCompleted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
    
    
    void OnGetLatestVersion(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
    void OnUpdateLatestVersion(FHttpRequestPtr Request, FHttpResponsePtr Respone, bool bWasSuccessful);
};
