// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CLOUDEYETOOLKIT_BackendService_generated_h
#error "BackendService.generated.h already included, missing '#pragma once' in BackendService.h"
#endif
#define CLOUDEYETOOLKIT_BackendService_generated_h

#define CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetData) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Action); \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_TagName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppVersion); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetData(Z_Param_Action,Z_Param_AppName,Z_Param_TagName,Z_Param_AppVersion); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRemoveTarget) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_TagName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RemoveTarget(Z_Param_AppName,Z_Param_TagName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAddTarget) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_TagName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_BundleName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_BundlePath); \
		P_GET_PROPERTY(UStrProperty,Z_Param_AssetFile); \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppVersion); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AddTarget(Z_Param_AppName,Z_Param_TagName,Z_Param_BundleName,Z_Param_BundlePath,Z_Param_AssetFile,Z_Param_AppVersion); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRemoveTable) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RemoveTable(Z_Param_AppName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateAppTable) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CreateAppTable(Z_Param_AppName); \
		P_NATIVE_END; \
	}


#define CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetData) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Action); \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_TagName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppVersion); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetData(Z_Param_Action,Z_Param_AppName,Z_Param_TagName,Z_Param_AppVersion); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRemoveTarget) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_TagName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RemoveTarget(Z_Param_AppName,Z_Param_TagName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAddTarget) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_TagName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_BundleName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_BundlePath); \
		P_GET_PROPERTY(UStrProperty,Z_Param_AssetFile); \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppVersion); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AddTarget(Z_Param_AppName,Z_Param_TagName,Z_Param_BundleName,Z_Param_BundlePath,Z_Param_AssetFile,Z_Param_AppVersion); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execRemoveTable) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->RemoveTable(Z_Param_AppName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateAppTable) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_AppName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CreateAppTable(Z_Param_AppName); \
		P_NATIVE_END; \
	}


#define CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABackendService(); \
	friend struct Z_Construct_UClass_ABackendService_Statics; \
public: \
	DECLARE_CLASS(ABackendService, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CloudEyeToolkit"), NO_API) \
	DECLARE_SERIALIZER(ABackendService)


#define CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_INCLASS \
private: \
	static void StaticRegisterNativesABackendService(); \
	friend struct Z_Construct_UClass_ABackendService_Statics; \
public: \
	DECLARE_CLASS(ABackendService, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CloudEyeToolkit"), NO_API) \
	DECLARE_SERIALIZER(ABackendService)


#define CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABackendService(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABackendService) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABackendService); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABackendService); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABackendService(ABackendService&&); \
	NO_API ABackendService(const ABackendService&); \
public:


#define CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABackendService(ABackendService&&); \
	NO_API ABackendService(const ABackendService&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABackendService); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABackendService); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABackendService)


#define CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_PRIVATE_PROPERTY_OFFSET
#define CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_10_PROLOG
#define CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_PRIVATE_PROPERTY_OFFSET \
	CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_RPC_WRAPPERS \
	CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_INCLASS \
	CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_PRIVATE_PROPERTY_OFFSET \
	CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_INCLASS_NO_PURE_DECLS \
	CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CloudEyeToolkit_Source_CloudEyeToolkit_Public_BackendService_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
