// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CloudEyeToolkit/Public/BackendService.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBackendService() {}
// Cross Module References
	CLOUDEYETOOLKIT_API UClass* Z_Construct_UClass_ABackendService_NoRegister();
	CLOUDEYETOOLKIT_API UClass* Z_Construct_UClass_ABackendService();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_CloudEyeToolkit();
	CLOUDEYETOOLKIT_API UFunction* Z_Construct_UFunction_ABackendService_AddTarget();
	CLOUDEYETOOLKIT_API UFunction* Z_Construct_UFunction_ABackendService_CreateAppTable();
	CLOUDEYETOOLKIT_API UFunction* Z_Construct_UFunction_ABackendService_GetData();
	CLOUDEYETOOLKIT_API UFunction* Z_Construct_UFunction_ABackendService_RemoveTable();
	CLOUDEYETOOLKIT_API UFunction* Z_Construct_UFunction_ABackendService_RemoveTarget();
// End Cross Module References
	void ABackendService::StaticRegisterNativesABackendService()
	{
		UClass* Class = ABackendService::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddTarget", &ABackendService::execAddTarget },
			{ "CreateAppTable", &ABackendService::execCreateAppTable },
			{ "GetData", &ABackendService::execGetData },
			{ "RemoveTable", &ABackendService::execRemoveTable },
			{ "RemoveTarget", &ABackendService::execRemoveTarget },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABackendService_AddTarget_Statics
	{
		struct BackendService_eventAddTarget_Parms
		{
			FString AppName;
			FString TagName;
			FString BundleName;
			FString BundlePath;
			FString AssetFile;
			FString AppVersion;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AppVersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetFile_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AssetFile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BundlePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_BundlePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BundleName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_BundleName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TagName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TagName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AppName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AppVersion_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AppVersion = { UE4CodeGen_Private::EPropertyClass::Str, "AppVersion", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventAddTarget_Parms, AppVersion), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AppVersion_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AppVersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AssetFile_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AssetFile = { UE4CodeGen_Private::EPropertyClass::Str, "AssetFile", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventAddTarget_Parms, AssetFile), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AssetFile_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AssetFile_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_BundlePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_BundlePath = { UE4CodeGen_Private::EPropertyClass::Str, "BundlePath", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventAddTarget_Parms, BundlePath), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_BundlePath_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_BundlePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_BundleName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_BundleName = { UE4CodeGen_Private::EPropertyClass::Str, "BundleName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventAddTarget_Parms, BundleName), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_BundleName_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_BundleName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_TagName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_TagName = { UE4CodeGen_Private::EPropertyClass::Str, "TagName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventAddTarget_Parms, TagName), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_TagName_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_TagName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AppName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AppName = { UE4CodeGen_Private::EPropertyClass::Str, "AppName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventAddTarget_Parms, AppName), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AppName_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AppName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABackendService_AddTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AppVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AssetFile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_BundlePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_BundleName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_TagName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_AddTarget_Statics::NewProp_AppName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_AddTarget_Statics::Function_MetaDataParams[] = {
		{ "Category", "HttpService" },
		{ "ModuleRelativePath", "Public/BackendService.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABackendService_AddTarget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABackendService, "AddTarget", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(BackendService_eventAddTarget_Parms), Z_Construct_UFunction_ABackendService_AddTarget_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_AddTarget_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABackendService_AddTarget_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_AddTarget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABackendService_AddTarget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABackendService_AddTarget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABackendService_CreateAppTable_Statics
	{
		struct BackendService_eventCreateAppTable_Parms
		{
			FString AppName;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AppName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_CreateAppTable_Statics::NewProp_AppName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_CreateAppTable_Statics::NewProp_AppName = { UE4CodeGen_Private::EPropertyClass::Str, "AppName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventCreateAppTable_Parms, AppName), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_CreateAppTable_Statics::NewProp_AppName_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_CreateAppTable_Statics::NewProp_AppName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABackendService_CreateAppTable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_CreateAppTable_Statics::NewProp_AppName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_CreateAppTable_Statics::Function_MetaDataParams[] = {
		{ "Category", "HttpService" },
		{ "ModuleRelativePath", "Public/BackendService.h" },
		{ "ToolTip", "UFUNCTION(BlueprintCallable, Category = \"HttpService\")\nstatic void CreateTableCommand(const FString &AppName, FString &Result);" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABackendService_CreateAppTable_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABackendService, "CreateAppTable", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(BackendService_eventCreateAppTable_Parms), Z_Construct_UFunction_ABackendService_CreateAppTable_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_CreateAppTable_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABackendService_CreateAppTable_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_CreateAppTable_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABackendService_CreateAppTable()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABackendService_CreateAppTable_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABackendService_GetData_Statics
	{
		struct BackendService_eventGetData_Parms
		{
			FString Action;
			FString AppName;
			FString TagName;
			FString AppVersion;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AppVersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TagName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TagName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AppName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Action_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Action;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_AppVersion_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_AppVersion = { UE4CodeGen_Private::EPropertyClass::Str, "AppVersion", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventGetData_Parms, AppVersion), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_AppVersion_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_AppVersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_TagName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_TagName = { UE4CodeGen_Private::EPropertyClass::Str, "TagName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventGetData_Parms, TagName), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_TagName_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_TagName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_AppName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_AppName = { UE4CodeGen_Private::EPropertyClass::Str, "AppName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventGetData_Parms, AppName), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_AppName_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_AppName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_Action_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_Action = { UE4CodeGen_Private::EPropertyClass::Str, "Action", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventGetData_Parms, Action), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_Action_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_Action_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABackendService_GetData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_AppVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_TagName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_AppName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_GetData_Statics::NewProp_Action,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_GetData_Statics::Function_MetaDataParams[] = {
		{ "Category", "HttpService" },
		{ "ModuleRelativePath", "Public/BackendService.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABackendService_GetData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABackendService, "GetData", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(BackendService_eventGetData_Parms), Z_Construct_UFunction_ABackendService_GetData_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_GetData_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABackendService_GetData_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_GetData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABackendService_GetData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABackendService_GetData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABackendService_RemoveTable_Statics
	{
		struct BackendService_eventRemoveTable_Parms
		{
			FString AppName;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AppName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_RemoveTable_Statics::NewProp_AppName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_RemoveTable_Statics::NewProp_AppName = { UE4CodeGen_Private::EPropertyClass::Str, "AppName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventRemoveTable_Parms, AppName), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_RemoveTable_Statics::NewProp_AppName_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_RemoveTable_Statics::NewProp_AppName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABackendService_RemoveTable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_RemoveTable_Statics::NewProp_AppName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_RemoveTable_Statics::Function_MetaDataParams[] = {
		{ "Category", "HttpService" },
		{ "ModuleRelativePath", "Public/BackendService.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABackendService_RemoveTable_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABackendService, "RemoveTable", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(BackendService_eventRemoveTable_Parms), Z_Construct_UFunction_ABackendService_RemoveTable_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_RemoveTable_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABackendService_RemoveTable_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_RemoveTable_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABackendService_RemoveTable()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABackendService_RemoveTable_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABackendService_RemoveTarget_Statics
	{
		struct BackendService_eventRemoveTarget_Parms
		{
			FString AppName;
			FString TagName;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TagName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TagName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AppName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::NewProp_TagName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::NewProp_TagName = { UE4CodeGen_Private::EPropertyClass::Str, "TagName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventRemoveTarget_Parms, TagName), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::NewProp_TagName_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::NewProp_TagName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::NewProp_AppName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::NewProp_AppName = { UE4CodeGen_Private::EPropertyClass::Str, "AppName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(BackendService_eventRemoveTarget_Parms, AppName), METADATA_PARAMS(Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::NewProp_AppName_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::NewProp_AppName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::NewProp_TagName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::NewProp_AppName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::Function_MetaDataParams[] = {
		{ "Category", "HttpService" },
		{ "ModuleRelativePath", "Public/BackendService.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABackendService, "RemoveTarget", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(BackendService_eventRemoveTarget_Parms), Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABackendService_RemoveTarget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABackendService_RemoveTarget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABackendService_NoRegister()
	{
		return ABackendService::StaticClass();
	}
	struct Z_Construct_UClass_ABackendService_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReceivedResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReceivedResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABackendService_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_CloudEyeToolkit,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABackendService_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABackendService_AddTarget, "AddTarget" }, // 2875531814
		{ &Z_Construct_UFunction_ABackendService_CreateAppTable, "CreateAppTable" }, // 2917701678
		{ &Z_Construct_UFunction_ABackendService_GetData, "GetData" }, // 2053781405
		{ &Z_Construct_UFunction_ABackendService_RemoveTable, "RemoveTable" }, // 2250566412
		{ &Z_Construct_UFunction_ABackendService_RemoveTarget, "RemoveTarget" }, // 1409918363
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABackendService_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BackendService.h" },
		{ "ModuleRelativePath", "Public/BackendService.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABackendService_Statics::NewProp_ReceivedResult_MetaData[] = {
		{ "Category", "HttpService" },
		{ "ModuleRelativePath", "Public/BackendService.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ABackendService_Statics::NewProp_ReceivedResult = { UE4CodeGen_Private::EPropertyClass::Str, "ReceivedResult", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ABackendService, ReceivedResult), METADATA_PARAMS(Z_Construct_UClass_ABackendService_Statics::NewProp_ReceivedResult_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABackendService_Statics::NewProp_ReceivedResult_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABackendService_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABackendService_Statics::NewProp_ReceivedResult,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABackendService_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABackendService>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABackendService_Statics::ClassParams = {
		&ABackendService::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ABackendService_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ABackendService_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ABackendService_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ABackendService_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABackendService()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABackendService_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABackendService, 3011541449);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABackendService(Z_Construct_UClass_ABackendService, &ABackendService::StaticClass, TEXT("/Script/CloudEyeToolkit"), TEXT("ABackendService"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABackendService);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
